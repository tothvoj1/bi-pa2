#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <cmath>
#include <cassert>
#include <iostream> 
#include <iomanip> 
#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <functional>
#include <memory>
using namespace std;
#endif /* __PROGTEST__ */


/*
  -------------------------------------------------
                  FULL NAME CLASS
  -------------------------------------------------
*/
class FullName{
  public:
    FullName( const string & name, const string & surname );

    string m_name;
    string m_surname;

    bool operator < ( const FullName & rhs ) const;
};

FullName::FullName(const string &name, const string &surname)
  :m_name(name), m_surname(surname)
{
  
}

bool FullName::operator<(const FullName &rhs) const
{
  if (m_surname == rhs.m_surname)
      return m_name < rhs.m_name;
  return m_surname < rhs.m_surname;
}

/*
  -------------------------------------------------
                    PERSON CLASS
  -------------------------------------------------
*/
class Person{
  public:
    Person( const string &name, const string &surname, const string &email, unsigned int salary );

    FullName& getFullname();
    string &  getName();
    string &  getSurname();
    string &  getEmail();
    uint      getSalary();

    void      setName( const string &name );
    void      setSurname( const string &surname );
    void      setEmail( const string &email );
    void      setSalary( const uint &name );
  private:
    FullName m_fullname;
    string m_email;
    uint m_salary;
};

Person::Person(const string &name, const string &surname, const string &email, unsigned int salary)
  : m_fullname({name, surname}), m_email(email), m_salary(salary)
{
}

FullName &Person::getFullname(){
    return m_fullname;
}

string &Person::getName(){
  return m_fullname.m_name;
}

string &Person::getSurname(){
  return m_fullname.m_surname;
}

string &Person::getEmail(){
  return m_email;
}

uint Person::getSalary(){
  return m_salary;
}

void Person::setName(const string &name){
  m_fullname.m_name = name;
}

void Person::setSurname(const string &surname){
  m_fullname.m_surname = surname;
}

void Person::setEmail(const string &email){
  m_email = email;
}

void Person::setSalary(const uint &salary){
  m_salary = salary;
}

/*
  -------------------------------------------------
                PERSONAL AGENDA CLASS
  -------------------------------------------------
*/
class CPersonalAgenda
{
  public:
    CPersonalAgenda  ( void );
    ~CPersonalAgenda ( void );
    bool add( const string& name, const string& surname, const string& email, unsigned int salary );
    
    bool del( const string& name, const string& surname );
    bool del( const string& email );
    
    bool changeName( const string& email, const string& newName, const string& newSurname );
    bool changeEmail( const string& name, const string& surname, const string& newEmail );
    
    bool setSalary( const string& name, const string& surname, unsigned int salary );
    bool setSalary ( const string& email, unsigned int salary );
    
    unsigned int getSalary( const string& name, const string& surname ) const;
    unsigned int getSalary( const string& email ) const;
    
    bool getRank ( const string& name, const string& surname, int& rankMin, int& rankMax ) const;
    bool getRank ( const string& email, int& rankMin, int& rankMax ) const;

    bool getFirst ( string& outName, string& outSurname ) const;
    bool getNext ( const string& name, const string& surname, string& outName, string& outSurname ) const;

    void printSummary();
  private:
    //members
    struct NamesRow;
    struct EmailsRow;
    struct SalaryRow;
    
    vector<SalaryRow> salaries_table; 
    vector<NamesRow>  names_table;
    vector<EmailsRow> emails_table;

    mutable vector<NamesRow>::const_iterator  last_names_it; //iterator of the last person searched by Name
    mutable vector<EmailsRow>::const_iterator last_emails_it; //iterator of the last person searched by Email

    //methods

    //searching in tables, both function return iterator returned by std::lower_bound and set found to false if name was not found
    vector<NamesRow>::const_iterator  findByName( bool& found, const string& name, const string& surname ) const;
    vector<EmailsRow>::const_iterator findByEmail( bool& found, const string& email ) const;

    bool updateSalaryTable( const uint & salary, bool increase = true );
    bool countRank(uint salary, int& rankMin, int& rankMax) const;
};

class CPersonalAgenda::NamesRow {
  public:
    FullName* m_fullName;
    Person*   m_person;

    friend bool operator < (const NamesRow& lhs, const FullName& rhs){
      return *(lhs.m_fullName) < rhs;
    }
    friend bool operator < (const FullName& lhs, const NamesRow& rhs){
      return lhs < *(rhs.m_fullName);
    }
    friend bool operator < (const NamesRow& lhs, const FullName* rhs){
      return *(lhs.m_fullName) < *rhs;
    }
    friend bool operator < (const FullName* lhs, const NamesRow& rhs){
      return *lhs < *(rhs.m_fullName);
    }
    friend bool operator < (const NamesRow& lhs, const NamesRow& rhs){
      return *(lhs.m_fullName) < *(rhs.m_fullName);
    }
};    

class CPersonalAgenda::EmailsRow {
  public:
    string*   m_email;
    Person*   m_person;
    
    EmailsRow( string * email, Person* person) : m_email(email), m_person( person ){ }
    
    friend bool operator < (const EmailsRow& lhs, const string & rhs){
      return *(lhs.m_email) < rhs;
    }
    friend bool operator < (const string & lhs, const EmailsRow& rhs){
      return lhs < *(rhs.m_email);
    }
    friend  bool operator < (const EmailsRow& lhs, const EmailsRow& rhs){
      return *(lhs.m_email) < *(rhs.m_email);
    }
};    

class CPersonalAgenda::SalaryRow {
  public:
    uint   m_salary;
    uint   m_count;
    
    SalaryRow( uint salary, uint count) : m_salary(salary), m_count( count ){ }
    
    friend bool operator < (const SalaryRow& lhs, const uint & rhs){
      return lhs.m_salary < rhs;
    }
    friend bool operator < (const uint & lhs, const SalaryRow& rhs){
      return lhs < rhs.m_salary;
    }
    friend bool operator < (const SalaryRow& lhs, const SalaryRow& rhs){
      return lhs.m_salary < rhs.m_salary;
    }
};    

vector<CPersonalAgenda::NamesRow>::const_iterator CPersonalAgenda::findByName(
  bool& found, const string &name, const string &surname) const
{
  found = true;
  //before binary search, try if the next person is not directly after the previous
  if( last_names_it < names_table.end() && last_names_it > names_table.begin() ){
    if( last_names_it->m_fullName->m_name == name && last_names_it->m_fullName->m_surname == surname ){
      return last_names_it;    
    }
  }
  
  FullName* fn = new FullName(name, surname);
  auto it = lower_bound(names_table.begin(), names_table.end(), fn);
  delete fn;

  if( it == names_table.end() || it->m_fullName->m_surname != surname || it->m_fullName->m_name != name )
    found = false;

  last_names_it = it;
  return it;
}

vector<CPersonalAgenda::EmailsRow>::const_iterator CPersonalAgenda::findByEmail(
  bool& found, const string &email) const
{
  found = true;
  if( last_emails_it < emails_table.end() && last_emails_it > emails_table.begin() ){
    if( *(last_emails_it->m_email) == email ){
      return last_emails_it;    
    }
  }

  auto it = lower_bound(emails_table.begin(), emails_table.end(), email);
  if( it == emails_table.end() || *(it->m_email) != email)
    found = false;

  last_emails_it = it;
  return it;
}

bool CPersonalAgenda::updateSalaryTable(const uint &salary, bool increase ){
  auto it = lower_bound(salaries_table.begin(), salaries_table.end(), salary);
  
  if( increase ){
    if( it == salaries_table.end() || it->m_salary != salary ){
      salaries_table.insert(it, {salary, 1});
      // sort( salaries_table.begin(), salaries_table.end());
    }
    else
      it->m_count++; //theres one more person with the same salary
  }
  else{
    if( it == salaries_table.end() || it->m_salary != salary ) //salary not in this table
      return false;
    else{
      if( it->m_count == 1 ){
        salaries_table.erase(it);
        //sort( salaries_table.begin(), salaries_table.end());
      }
      else
        it->m_count--; 
    }  
  }
  return true;
}

bool CPersonalAgenda::countRank(uint salary, int &rankMin, int &rankMax) const
{
  size_t min = 0;
  size_t max = 0;

  for( auto a : salaries_table ){
    if( salary > a.m_salary )
      min += a.m_count;
    else if( salary == a.m_salary ){
      max = a.m_count;
      break;
    } 
  }
  rankMin = min;
  rankMax = min + max - 1;
  
  return true;
}

CPersonalAgenda::CPersonalAgenda(void)
{
}

CPersonalAgenda::~CPersonalAgenda(void)
{
  for( auto & a : emails_table )
    delete a.m_person;
}

bool CPersonalAgenda::add(const string &name, const string &surname, const string &email, unsigned int salary)
{
  bool name_found, email_found;
  auto name_it = findByName( name_found, name, surname );
  if( name_found ){
    // cout<<name<<" "<<surname<<" is already in database!"<<endl;
    return false;
  }
  
  auto email_it = findByEmail( email_found, email );
  if( email_found ){
    // cout<<email<<" is already in database!"<<endl;
    return false;
  }

  Person* p = new Person( name, surname, email, salary );

  EmailsRow e = { &(p->getEmail()), p };
  emails_table.insert( email_it, e );
  // emails_table.push_back( e );
  // sort( emails_table.begin(), emails_table.end() );

  NamesRow n = { &(p->getFullname()), p };
  names_table.insert( name_it, n );
  // names_table.push_back( n );
  // sort( names_table.begin(), names_table.end() );

  updateSalaryTable( salary );

  return true;
}

bool CPersonalAgenda::del(const string &name, const string &surname)
{
  bool name_found, email_found;
  auto name_it = findByName( name_found, name, surname );
  if( !name_found )
    return false;

  auto mail_it = findByEmail( email_found, name_it->m_person->getEmail() );

  updateSalaryTable( name_it->m_person->getSalary(), false );
  
  delete name_it->m_person;
  names_table.erase(name_it);
  emails_table.erase(mail_it);

  return true;
}

bool CPersonalAgenda::del(const string &email)
{
  bool email_found;
  
  auto mail_it = findByEmail( email_found, email );
  if( !email_found )
    return false;

  auto name_it = findByName( email_found, mail_it->m_person->getName(), mail_it->m_person->getSurname() );

  updateSalaryTable( name_it->m_person->getSalary(), false );
  
  delete name_it->m_person;
  names_table.erase(name_it);
  emails_table.erase(mail_it);

  return true;
}

bool CPersonalAgenda::changeName(const string &email, const string &newName, const string &newSurname)
{
  bool name_found, email_found;
  //find if exists
  auto email_it = findByEmail( email_found, email );
  if( !email_found )
    return false;
  //find if new doesnt exists
  auto new_name_it = findByName( name_found, newName, newSurname );
  if( name_found )
    return false;
  //get old name
  auto p = email_it->m_person;
  //delete old name
  auto old_n_it = findByName( name_found, p->getName(), p->getSurname() );
  names_table.erase( old_n_it );
  //set new name
  p->setName(newName);
  p->setSurname(newSurname);
  NamesRow new_n = { &(p->getFullname()), p };
  //insert new name, iterator invalidated by delete
  new_name_it = findByName( name_found, newName, newSurname );
  names_table.insert(new_name_it, new_n );

  // sort( names_table.begin(), names_table.end() );
  return true;
}

bool CPersonalAgenda::changeEmail(const string &name, const string &surname, const string &newEmail)
{
  bool name_found, email_found;
  //find if exists
  auto name_it = findByName( name_found, name, surname );
  if( !name_found )
    return false;
  //find if new doesnt exists
  auto new_email_it = findByEmail( email_found, newEmail );
  if( email_found )
    return false;
  //get old email
  auto p = name_it->m_person;
  //delete old email
  auto old_e_it = findByEmail( email_found, p->getEmail() );
  emails_table.erase( old_e_it );
  //set new name
  p->setEmail( newEmail );

  EmailsRow new_e = { &(p->getEmail()), p };
  //insert new name, iterator invalidated by delete
  new_email_it = findByEmail( name_found, newEmail );
  emails_table.insert(new_email_it, new_e );

  // sort( names_table.begin(), names_table.end() );
  return true;
}

bool CPersonalAgenda::setSalary(const string &name, const string &surname, unsigned int salary)
{
  bool n_found;
  auto it = findByName( n_found, name, surname );
  
  if( !n_found  )
    return false;

  updateSalaryTable( it->m_person->getSalary(), false );
  it->m_person->setSalary( salary );
  updateSalaryTable( salary );

  return true;
}

bool CPersonalAgenda::setSalary(const string &email, unsigned int salary)
{
  bool e_found;
  auto it = findByEmail( e_found, email );
  
  if( !e_found  )
    return false;

  updateSalaryTable( it->m_person->getSalary(), false );
  it->m_person->setSalary( salary );
  updateSalaryTable( salary );

  return true;
}

unsigned int CPersonalAgenda::getSalary(const string &name, const string &surname) const
{
  bool n_f;
  auto it = findByName( n_f, name, surname );
  
  if( !n_f )
    return 0;
  return it->m_person->getSalary();
}

unsigned int CPersonalAgenda::getSalary(const string &email) const
{
  bool e_f;
  auto it = findByEmail( e_f, email );
  
  if( !e_f  )
    return 0;
  return it->m_person->getSalary();
}

bool CPersonalAgenda::getRank(const string &name, const string &surname, int &rankMin, int &rankMax) const
{
  bool n_f;
  auto it = findByName( n_f, name, surname );
  
  if( !n_f  )
    return false;
  uint salary = it->m_person->getSalary();
  
  return countRank(salary, rankMin, rankMax);
}

bool CPersonalAgenda::getRank(const string &email, int &rankMin, int &rankMax) const
{
  bool e_f;
  auto it = findByEmail( e_f, email );

  if( !e_f  )
    return false;
  uint salary = it->m_person->getSalary();

  return countRank(salary, rankMin, rankMax);
}

bool CPersonalAgenda::getFirst(string &outName, string &outSurname) const
{
  if( names_table.empty() )
    return false;
  
  last_names_it = names_table.begin();
  
  outName = names_table[0].m_fullName->m_name;
  outSurname = names_table[0].m_fullName->m_surname;

  return true;
}

bool CPersonalAgenda::getNext(const string &name, const string &surname, string &outName, string &outSurname) const
{
  bool n_f;
  auto it = findByName( n_f, name, surname );
  
  if( !n_f  )
    return false;
  if( ++it != names_table.end() ){
    outName = it->m_person->getName();
    outSurname = it->m_person->getSurname();
    last_names_it = it;
    return true;
  }
  return false;
}

void CPersonalAgenda::printSummary()
{
  cout<<"Name Table:"<<endl;
  for( auto& a : names_table ){
    cout<<"\t"<< a.m_fullName->m_name <<" "<<a.m_fullName->m_surname <<" : "
    <<a.m_person->getName()<<", "<<a.m_person->getSurname()<<", "<< a.m_person->getEmail()<<", "<<a.m_person->getSalary()<<endl;
  }
  cout<<"Email Table:"<<endl;
  for( auto& a : emails_table ){
    cout<<"\t"<< *(a.m_email)<<" : "
    <<a.m_person->getName()<<", "<<a.m_person->getSurname()<<", "<< a.m_person->getEmail()<<", "<<a.m_person->getSalary()<<endl;
  }
  cout<<"Salary Table:"<<endl;
  for( auto& a : salaries_table ){
    cout<<"\t"<< a.m_salary<<", # "<< a.m_count<<endl;
  }
}

#ifndef __PROGTEST__
int main(void)
{
  string outName, outSurname;
  int lo, hi;

  CPersonalAgenda b1;
  assert ( b1 . add ( "John", "Smith", "john", 30000 ) );
  b1.printSummary();
  assert ( b1 . add ( "John", "Miller", "johnm", 35000 ) );
  b1.printSummary();
  assert ( b1 . add ( "Peter", "Smith", "peter", 23000 ) );
  b1.printSummary();
  assert ( b1 . getFirst ( outName, outSurname )
           && outName == "John"
           && outSurname == "Miller" );
  assert ( b1 . getNext ( "John", "Miller", outName, outSurname )
           && outName == "John"
           && outSurname == "Smith" );
  assert ( b1 . getNext ( "John", "Smith", outName, outSurname )
           && outName == "Peter"
           && outSurname == "Smith" );
  assert ( ! b1 . getNext ( "Peter", "Smith", outName, outSurname ) );
  assert ( b1 . setSalary ( "john", 32000 ) );
  b1.printSummary();
  assert ( b1 . getSalary ( "john" ) ==  32000 );
  assert ( b1 . getSalary ( "John", "Smith" ) ==  32000 );
  assert ( b1 . getRank ( "John", "Smith", lo, hi )
           && lo == 1
           && hi == 1 );
  assert ( b1 . getRank ( "john", lo, hi )
           && lo == 1
           && hi == 1 );
  assert ( b1 . getRank ( "peter", lo, hi )
           && lo == 0
           && hi == 0 );
  assert ( b1 . getRank ( "johnm", lo, hi )
           && lo == 2
           && hi == 2 );
  assert ( b1 . setSalary ( "John", "Smith", 35000 ) );
  assert ( b1 . getSalary ( "John", "Smith" ) ==  35000 );
  assert ( b1 . getSalary ( "john" ) ==  35000 );
  assert ( b1 . getRank ( "John", "Smith", lo, hi )
           && lo == 1
           && hi == 2 );
  assert ( b1 . getRank ( "john", lo, hi )
           && lo == 1
           && hi == 2 );
  assert ( b1 . getRank ( "peter", lo, hi )
           && lo == 0
           && hi == 0 );
  assert ( b1 . getRank ( "johnm", lo, hi )
           && lo == 1
           && hi == 2 );
  assert ( b1 . changeName ( "peter", "James", "Bond" ) );
  assert ( b1 . getSalary ( "peter" ) ==  23000 );
  assert ( b1 . getSalary ( "James", "Bond" ) ==  23000 );
  assert ( b1 . getSalary ( "Peter", "Smith" ) ==  0 );
  assert ( b1 . getFirst ( outName, outSurname )
           && outName == "James"
           && outSurname == "Bond" );
  assert ( b1 . getNext ( "James", "Bond", outName, outSurname )
           && outName == "John"
           && outSurname == "Miller" );
  assert ( b1 . getNext ( "John", "Miller", outName, outSurname )
           && outName == "John"
           && outSurname == "Smith" );
  assert ( ! b1 . getNext ( "John", "Smith", outName, outSurname ) );
  assert ( b1 . changeEmail ( "James", "Bond", "james" ) );
  assert ( b1 . getSalary ( "James", "Bond" ) ==  23000 );
  assert ( b1 . getSalary ( "james" ) ==  23000 );
  assert ( b1 . getSalary ( "peter" ) ==  0 );
  assert ( b1 . del ( "james" ) );
  assert ( b1 . getRank ( "john", lo, hi )
           && lo == 0
           && hi == 1 );
  assert ( b1 . del ( "John", "Miller" ) );
  assert ( b1 . getRank ( "john", lo, hi )
           && lo == 0
           && hi == 0 );
  assert ( b1 . getFirst ( outName, outSurname )
           && outName == "John"
           && outSurname == "Smith" );
  assert ( ! b1 . getNext ( "John", "Smith", outName, outSurname ) );
  assert ( b1 . del ( "john" ) );
  assert ( ! b1 . getFirst ( outName, outSurname ) );
  assert ( b1 . add ( "John", "Smith", "john", 31000 ) );
  assert ( b1 . add ( "john", "Smith", "joHn", 31000 ) );
  assert ( b1 . add ( "John", "smith", "jOhn", 31000 ) );

  CPersonalAgenda b2;
  assert ( ! b2 . getFirst ( outName, outSurname ) );
  assert ( b2 . add ( "James", "Bond", "james", 70000 ) );
  assert ( b2 . add ( "James", "Smith", "james2", 30000 ) );
  assert ( b2 . add ( "Peter", "Smith", "peter", 40000 ) );
  assert ( ! b2 . add ( "James", "Bond", "james3", 60000 ) );
  assert ( ! b2 . add ( "Peter", "Bond", "peter", 50000 ) );
  assert ( ! b2 . changeName ( "joe", "Joe", "Black" ) );
  assert ( ! b2 . changeEmail ( "Joe", "Black", "joe" ) );
  assert ( ! b2 . setSalary ( "Joe", "Black", 90000 ) );
  assert ( ! b2 . setSalary ( "joe", 90000 ) );
  assert ( b2 . getSalary ( "Joe", "Black" ) ==  0 );
  assert ( b2 . getSalary ( "joe" ) ==  0 );
  assert ( ! b2 . getRank ( "Joe", "Black", lo, hi ) );
  assert ( ! b2 . getRank ( "joe", lo, hi ) );
  assert ( ! b2 . changeName ( "joe", "Joe", "Black" ) );
  assert ( ! b2 . changeEmail ( "Joe", "Black", "joe" ) );
  assert ( ! b2 . del ( "Joe", "Black" ) );
  assert ( ! b2 . del ( "joe" ) );
  assert ( ! b2 . changeName ( "james2", "James", "Bond" ) );
  assert ( ! b2 . changeEmail ( "Peter", "Smith", "james" ) );
  assert ( ! b2 . changeName ( "peter", "Peter", "Smith" ) );
  assert ( ! b2 . changeEmail ( "Peter", "Smith", "peter" ) );
  assert ( b2 . del ( "Peter", "Smith" ) );
  assert ( ! b2 . changeEmail ( "Peter", "Smith", "peter2" ) );
  assert ( ! b2 . setSalary ( "Peter", "Smith", 35000 ) );
  assert ( b2 . getSalary ( "Peter", "Smith" ) ==  0 );
  assert ( ! b2 . getRank ( "Peter", "Smith", lo, hi ) );
  assert ( ! b2 . changeName ( "peter", "Peter", "Falcon" ) );
  assert ( ! b2 . setSalary ( "peter", 37000 ) );
  assert ( b2 . getSalary ( "peter" ) ==  0 );
  assert ( ! b2 . getRank ( "peter", lo, hi ) );
  assert ( ! b2 . del ( "Peter", "Smith" ) );
  assert ( ! b2 . del ( "peter" ) );
  assert ( b2 . add ( "Peter", "Smith", "peter", 40000 ) );
  assert ( b2 . getSalary ( "peter" ) ==  40000 );

  b2.printSummary();

  return EXIT_SUCCESS;
}
#endif /* __PROGTEST__ */