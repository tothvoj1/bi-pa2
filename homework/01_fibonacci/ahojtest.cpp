#ifndef __PROGTEST__
#include <cstdio>

#include <cstdlib>
#include <cstring>
#include <cstdint>
#include <cassert>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
using namespace std;
#endif /* __PROGTEST__ */

class FibonacciCode{
  public:
    FibonacciCode(const char* inFile, const char* outFile);
    bool fibToUtf8();
    bool 
    utf8ToFib();
  private:
    string m_in_file_name; 
    string m_out_file_name;

    ifstream m_in;
    ofstream m_out;

    size_t bitPos;
  
    //vector<char> buffer;
    char lastByte;

    vector<size_t> sequense;
    //convert val to utf8 codes and appends them to out stream
    bool valueToUtf8( size_t val );
    bool valueToFib( size_t val );
};

FibonacciCode::FibonacciCode(const char *inFile, const char *outFile)
  : m_in_file_name(inFile), m_out_file_name (outFile), /*buffer({0}),*/ bitPos(0), lastByte(0){
  size_t prev = 1;
  size_t prev_prev = 0;
  size_t curr = 0;
  for( size_t i = 0; i < 31; i++ ){  
    curr = prev + prev_prev;
    prev_prev = prev;
    prev = curr;

    sequense.push_back(curr);
  }
}

bool FibonacciCode::fibToUtf8()
{
  m_in.open( m_in_file_name, fstream::binary );
  m_out.open( m_out_file_name, ostream::binary  );
  if( !((m_in.good() || m_in.eof()) && (m_out.good() || m_out.eof())) ){
    //cerr << "didn't open" << endl;
    return false;
  }

  char c;
  size_t val = 0, j = 0;
  bool one = false, zeroes = true;
  while( m_in.get(c) ){
    for( size_t i = 0; i < 8; i++ ){
      if((c >> i) & 1){
        zeroes = false;
        if(one){ //second 1 in row means end of curr char
          val--; 
          if(!valueToUtf8( val ))
            return false;
          
          one = false;
          zeroes = true;
          val = 0;
          j = -1;
        }
        else{
          val += sequense[j];
          one = true;
        }
      }    
      else
        one = false;

      j++;
    }
  }

  if(!((m_in.good() || m_in.eof()) && (m_out.good() || m_out.eof())))
    return false;
  
  if ( !zeroes ){
    //cout << "wheres padding pal?";
    return false;
  }

  m_in.close();
  m_out.close();
  return  (m_in.good() || m_in.eof()) && (m_out.good() || m_out.eof());
}
void printByte(uint8_t b){
  for(size_t i= 0; i < 8; i++)
    cout<< ((b>>(7-i))&1);
  cout<<endl;
}
bool FibonacciCode::utf8ToFib()
{
  m_in.open( m_in_file_name );
  m_out.open( m_out_file_name );
  if( !((m_in.good() || m_in.eof()) && (m_out.good() || m_out.eof())) )
    return false;
  
    
  vector<uint8_t> start_prefix = {0b11110000, 0b11100000, 0b11000000, 0b00000000};
  vector<size_t>  shifts = {3, 4, 5, 7};
  vector<uint8_t> start_masks =  {0b00000111, 0b00001111, 0b00011111, 0b01111111};

  uint8_t prefix = 0b10000000;
  size_t  shift = 6;
  uint8_t mask = 0b00111111;
  
  char b;
  size_t val = 0, count = 4, j = 0;
  bool begin = true;
  while( m_in.get(b) ){
    uint8_t c = (uint8_t)b;

    if( begin ){
      for( size_t i = 0; i < 4; i++ ){
        uint8_t prefix_l = c >> shifts[i] ;
        uint8_t prefix_r = start_prefix[i]  >> shifts[i];

        if( prefix_l == prefix_r ){
          val = c & start_masks[i];
          count = 3 - i;
          j = 0;
          
          begin = false;
          i = 4;
        }
        else count = 4;
      }
      if( count >= 4 )
        return false;
    }
    else{
      if( (c >> shift) != (prefix >> shift) ){
        return false;
      }
      val = (val << shift) | ( c & mask );
      j++;
    }

    if( j == count ){
      
      if( count > 0 && val == 0){
        return false;
      }
      
      if(( count == 3 && val < 0x10000  )
        ||(count == 2 && val < 0x800 )
        ||(count == 1 && val < 0x80 )){
          return false;
      }
      
      
      if( !valueToFib( val ) ){
        return false;
      }
      val = 0;
      j = 0;
      count = 4;

      begin = true;      
    }
  }
  
  if(!((m_in.good() || m_in.eof()) && (m_out.good() || m_out.eof())))
    return false;

  printByte( lastByte );
  
  if( lastByte != 0 )
    m_out << lastByte;
  
  m_in.close();
  m_out.close();
  return (m_in.good() || m_in.eof()) && (m_out.good() || m_out.eof());
}

bool FibonacciCode::valueToUtf8(size_t val)
{
  vector<uint8_t> bytes, masks;
  //decide length
  if( val <= 0x7F  ){
    bytes = {0b00000000};
    masks = {0b01111111};
  }
  else if ( val <= 0x7FF ){
    bytes = {0b10000000, 0b11000000}; 
    masks = {0b00111111, 0b00011111}; 
  }
  else if ( val <= 0xFFFF ){
    bytes = {0b10000000, 0b10000000, 0b11100000};
    masks = {0b00111111, 0b00111111, 0b00001111};
  } 
  else if ( val <= 0x10FFFF ){
    bytes = {0b10000000, 0b10000000, 0b10000000, 0b11110000}; 
    masks = {0b00111111, 0b00111111, 0b00111111, 0b00000111};
  }
  else 
    return false;

  for(size_t i = 0; i < bytes.size(); i++){
    uint8_t chunk = (val >> 6 * i) & masks[i]; 
    bytes[i] = bytes[i] | chunk;
  }

  for( auto it = bytes.rbegin(); it != bytes.rend(); it++ )
    m_out << *it;

  return true;
}

bool FibonacciCode::valueToFib(size_t val){
  val++;
  vector<bool> valInFib = {1};

  auto it = sequense.rbegin();
  bool begin = false;

  while (val != 0){
    if( val >= *it ){
      begin = true;
      val -= *it;
      valInFib.push_back(true);
    }
    else if(begin)
      valInFib.push_back(false);
    it++;
  }
  while (it != sequense.rend())  {
    valInFib.push_back(false);
    it++;
  }
  
  for( auto a = valInFib.rbegin(); a != valInFib.rend(); a++ ){
    if(*a)
      lastByte = lastByte | (1 << bitPos );
    bitPos++;
    if( bitPos == 8){
      m_out << lastByte;
      if( !((m_in.good() || m_in.eof()) && (m_out.good() || m_out.eof())) )
        return false;
      bitPos = 0;
      lastByte = 0;
    }
  }
  return true;
}



bool utf8ToFibonacci( const char* inFile, const char* outFile ){
  FibonacciCode f( inFile, outFile );
  //cout << endl << "start utf8ToFibonacci"<<endl;
  bool b = f.utf8ToFib();
  return b;
}

bool fibonacciToUtf8( const char* inFile, const char* outFile )
{
  FibonacciCode f( inFile, outFile );
  bool b = f.fibToUtf8();
  return b;
}
  
#ifndef __PROGTEST__

bool               identicalFiles                          ( const char      * file1,
                                                             const char      * file2 )
{
  //return love;
  return false;
}

int main ( int argc, char * argv [] )
{
  fibonacciToUtf8("in_5077759.bin", "output.utf8");
  
  fibonacciToUtf8( "example/dst_0.fib", "output0.utf8" );
  utf8ToFibonacci( "example/src_0.utf8", "output0.fib");
  fibonacciToUtf8( "example/dst_1.fib", "output1.utf8" );
  utf8ToFibonacci( "example/src_1.utf8", "output1.fib");
  fibonacciToUtf8( "example/dst_2.fib", "output2.utf8" );
  utf8ToFibonacci( "example/src_2.utf8", "output2.fib");
  fibonacciToUtf8( "example/dst_3.fib", "output3.utf8" );
  utf8ToFibonacci( "example/src_3.utf8", "output3.fib");
  fibonacciToUtf8( "example/dst_4.fib", "output4.utf8" );
  utf8ToFibonacci( "example/src_4.utf8", "output4.fib");
  fibonacciToUtf8( "example/dst_5.fib", "output5.utf8" );
  utf8ToFibonacci( "example/src_5.utf8", "output5.fib");
  fibonacciToUtf8( "example/dst_6.fib", "output6.utf8" );
  utf8ToFibonacci( "example/src_6.utf8", "output6.fib");
  fibonacciToUtf8( "example/dst_7.fib", "output7.utf8" );
  utf8ToFibonacci( "example/src_7.utf8", "output7.fib");
  fibonacciToUtf8( "example/dst_10.fib", "output8.utf8" );
  //         && identicalFiles ( "output.fib", "example/dst_0.fib" ) );
  //assert ( utf8ToFibonacci ( "example/src_1.utf8", "output.fib" )
  //         && identicalFiles ( "output.fib", "example/dst_1.fib" ) );
  //assert ( utf8ToFibonacci ( "example/src_2.utf8", "output.fib" )
  //         && identicalFiles ( "output.fib", "example/dst_2.fib" ) );
  //assert ( utf8ToFibonacci ( "example/src_3.utf8", "output.fib" )
  //         && identicalFiles ( "output.fib", "example/dst_3.fib" ) );
  //assert ( utf8ToFibonacci ( "example/src_4.utf8", "output.fib" )
  //         && identicalFiles ( "output.fib", "example/dst_4.fib" ) );
  //assert ( ! utf8ToFibonacci ( "example/src_5.utf8", "output.fib" ) );
  //assert ( fibonacciToUtf8 ( "example/src_6.fib", "output.utf8" )
  //         && identicalFiles ( "output.utf8", "example/dst_6.utf8" ) );
  //assert ( fibonacciToUtf8 ( "example/src_7.fib", "output.utf8" )
  //         && identicalFiles ( "output.utf8", "example/dst_7.utf8" ) );
  //assert ( fibonacciToUtf8 ( "example/src_8.fib", "output.utf8" )
  //         && identicalFiles ( "output.utf8", "example/dst_8.utf8" ) );
  //assert ( fibonacciToUtf8 ( "example/src_9.fib", "output.utf8" )
  //         && identicalFiles ( "output.utf8", "example/dst_9.utf8" ) );
  //assert ( fibonacciToUtf8 ( "example/src_10.fib", "output.utf8" )
  //         && identicalFiles ( "output.utf8", "example/dst_10.utf8" ) );
  //assert ( ! fibonacciToUtf8 ( "example/src_11.fib", "output.utf8" ) );
 
  return EXIT_SUCCESS;
}
#endif /* __PROGTEST__ */


