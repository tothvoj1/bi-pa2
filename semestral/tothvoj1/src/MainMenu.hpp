#pragma once

#include "Scene.hpp"
#include "GameObject.hpp"
#include <vector>
#include <map>


class MainMenu : public Scene
{
  public:
    MainMenu();
    ~MainMenu();

    virtual bool    processInput(); //reads buttons
    virtual Scene*  update( const float& tick);   //continues to another scene, either save select and load from file, or generating new game
    virtual void    render( SDL_Renderer* renderer );   //shows menu  
  
    MainMenu& addButton( GameObject* button, Scene* scene );
    MainMenu& setUnderscore( GameObject* button );
  private:
    std::vector<GameObject*>  m_buttons;
    GameObject*               m_underscore;
    std::vector<Scene*>       m_scenes;
        
    int                       m_selected;
    int                       m_next_choice;
    bool                      m_next_scene;

    const float               m_button_reload = 0.3 * 1000.0;
    float                     m_next_time = 0; 
};