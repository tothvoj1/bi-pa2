#include "Tile.hpp"

Tile::Tile()
  : GameObject()
{
}

Tile::Tile(const Texture &t, int x, int y, const std::string &tag)
  : GameObject(t,x,y,tag)
{
}

Tile::~Tile()
{
}

bool Tile::isSafe() const
{
  return true;
}

bool Tile::isWalkable() const
{
  return false;
}

bool Tile::isPlayerOnly() const
{
  return true;
}
