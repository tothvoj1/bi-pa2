#include "Player.hpp"

Player::Player()
 : Character(), m_direction({0,0})
{
}

Player::Player(const Texture &t, int x, int y, int max_lives, double speed, std::unique_ptr<Weapon> weapon)
  : Character(t,x,y, "player", speed, max_lives), IWeaponized(std::move(weapon)), m_direction({0,0}),
  m_shooting_direction({0,0}), m_shooting(false)
{
}

Player::~Player()
{
}

Player & Player::step(const Direction &direction)
{
  switch (direction)
  {
    case LEFT:
      m_direction.x = m_direction.x == -1 ? -1 : m_direction.x - 1;
      break;
    case RIGHT:
      m_direction.x = m_direction.x == 1 ? 1 : m_direction.x + 1;
      break;
    case UP:
      m_direction.y = m_direction.y == -1 ? -1 : m_direction.y - 1;
      break;
    case DOWN:
      m_direction.y = m_direction.y == 1 ? 1 : m_direction.y + 1;
      break;
  }
  return *this;
}

Player & Player::move()
{
  auto norm =  m_direction.norm();
  m_coords += m_speed * norm;

  return *this;
}

Player &Player::beginShoot(const Vector2D& direction )
{
  m_shooting = true;
  m_shooting_direction = direction;
  return *this;
}

Player &Player::endShoot(const Vector2D& direction )
{
  if( m_shooting_direction == direction )
    m_shooting = false;
  return *this;
}

bool Player::isShooting()
{
  return m_shooting;
}

IWeaponized &Player::shoot(std::list<std::unique_ptr<Projectile>> &bullet_list)
{
  bullet_list.merge( m_weapon->fire( getCenter(), m_shooting_direction , false, true ) );
  return *this;
}

Player &Player::updateTimers(const float &tick)
{
  Character::updateTimers(tick);
  m_weapon->updateCooldown(tick);

  return *this;
}
