#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <iostream>
#include <string>
#include <memory>

class Texture {
  public:
    Texture();
    Texture(const std::string& path, SDL_Renderer* renderer_ptr);
    ~Texture();

    /// @brief Loads 
    /// @param path
    /// @return false if texture is already loaded, true when loaded succesfully
    bool loadTexture( const std::string& path, SDL_Renderer* renderer_ptr );

    void render(int x, int y, SDL_Renderer* r);

    int getWidth()  const;
    int getHeight() const;
  private: 
    // SDL_Texture*  m_texture;
    std::shared_ptr<SDL_Texture> m_texture;
    int m_width;
    int m_height;
};


