#include "Melee.hpp"


Melee::Melee() :Enemy()
{
  m_tag = "Melee";
}

Melee::Melee(const Texture &t, int x, int y, double speed, int max_lives, double attack_range, int attack_cooldown, int attack_damage)
  : Enemy(t, x, y, "Melee", speed, max_lives, attack_range, attack_cooldown, attack_damage )
{
}

Enemy &Melee::attack(std::list<std::unique_ptr<Projectile>> &projectile_list)
{
  auto target_a = m_target->getArea();
  auto attack_a = getArea();
  attack_a.w += m_attack_range;
  attack_a.h += m_attack_range;
  if( attack_a.isColliding(target_a) )
    m_target->reduceHealth( m_attack_damage );

  return *this;
}

Melee & Melee::move()
{
  Vector2D arrow = getCenter().distanceVec(m_target->getCenter());
  m_coords += m_speed * arrow.norm();

  return *this;
}
