#pragma once

#include <list>
#include <memory>
#include "Projectile.hpp"

class Weapon{
  public:
    Weapon();
    Weapon(const std::shared_ptr<Projectile>& projectile, float reload_time);
    virtual ~Weapon();
    virtual std::list<std::unique_ptr<Projectile>> fire( const Vector2D& origin, const Vector2D& direction, bool ignoreEnemy, bool ignorePlayer );
    Weapon& updateCooldown(const float& tick);
  protected:
    std::shared_ptr<Projectile>  m_projectile;
    
    float       reload_time;
    float       cooldown_left;
};