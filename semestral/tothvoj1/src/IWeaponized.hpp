#pragma once

#include "Weapon.hpp"

class IWeaponized{
  public:
    IWeaponized( );
    IWeaponized( std::unique_ptr<Weapon> weapon );
    IWeaponized( const IWeaponized &other );
    IWeaponized( IWeaponized &&other );
    virtual ~IWeaponized();
    /// @brief Tell's bearer to shoot in some direction, storing the projectiles in a list for further use
    /// @param bullet_list reference to list where all fired projectiles will be passed
    /// @param direction vector pointing to where you want to shoot. Will be normed, doesn't affect speed
    /// @return Reference to the caller instance
    virtual IWeaponized& shoot( std::list<std::unique_ptr<Projectile>>& bullet_list ) = 0;
  protected:
    std::unique_ptr<Weapon> m_weapon;
};