#pragma once

#include <ostream>
#include <cmath>

class Vector2D
{
  public:
    Vector2D();
    Vector2D(const Vector2D& other);
    Vector2D(double x, double y);
    Vector2D(int x, int y);

    double x;
    double y;

    /// @brief Computes lenght of the vector using pythagoras theorem
    /// @return length as double
    double    length() const;

    /// @brief Norms the vector so its lenght is 1
    /// @return Vector with length of 1 pointin in the same direction as original vector
    Vector2D  norm() const;
    
    /// @brief Using this as point A and other as point B, computes vector pointing from A to B.
    /// @param other 
    /// @return Vector used to get direction and distance of A to B
    Vector2D  distanceVec(const Vector2D& other) const;

    Vector2D operator +( const Vector2D& other ) const;
    Vector2D operator -( const Vector2D& other ) const;
    
    Vector2D& operator +=( const Vector2D& other );
    Vector2D& operator -=( const Vector2D& other );

    bool operator ==( const Vector2D& other ) const;

    friend std::ostream& operator <<( std::ostream & os, const Vector2D& v );
};

Vector2D operator *( const double& scalar, const Vector2D& other );
Vector2D operator *( const int& scalar, const Vector2D& other );

// Vector2D& operator *=( const double& scalar, const Vector2D& other );
// Vector2D& operator *=( const int& scalar, const Vector2D& other );