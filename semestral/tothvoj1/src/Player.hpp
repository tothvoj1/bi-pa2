#pragma once

#include "Character.hpp"
#include "EDirection.hpp"
#include "IWeaponized.hpp"

class Player : public Character, public IWeaponized
{
  public:
    Player();
    Player( const Texture& t, int x, int y, int max_lives, double speed,  std::unique_ptr<Weapon> weapon );
    ~Player();
    
    Player& step( const Direction& direction );
    virtual Player& move();
    
    Player& beginShoot(const Vector2D& direction);
    Player& endShoot(const Vector2D& direction);
    bool isShooting();
    virtual IWeaponized& shoot( std::list<std::unique_ptr<Projectile>>& bullet_list);
    Player& updateTimers( const float& tick ) override;

  private:
    Vector2D  m_direction;
    Vector2D  m_shooting_direction;
    bool      m_shooting;
};