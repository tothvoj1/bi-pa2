#include "Range.hpp"

Range::Range() : Enemy(), IWeaponized(), close_enough(false)
{
  srand (time(NULL));
  m_tag = "Range";
}

Range::Range(const Texture &t, int x, int y, double speed, int max_lives, double attack_range, int attack_cooldown, 
              int attack_damage, std::unique_ptr<Weapon> weapon)
  : Enemy(t,x,y,"Range", speed,max_lives,attack_range,attack_cooldown,attack_damage), 
    IWeaponized(std::move(weapon)), 
    close_enough(false),
    walk_countdown(),
    walk_countdown_left()
{
}

Range & Range::updateTimers(const float & tick)
{
  Character::updateTimers(tick);
  m_attack_cooldown_left = m_attack_cooldown_left < 0 
    ? 0 : m_attack_cooldown_left - tick;
  walk_countdown_left -= tick;
  m_weapon->updateCooldown(tick);

  return *this;
}

Range & Range::move()
{
  if( m_target == nullptr )
    return *this;

  auto proximity = m_attack_range; //if too far get closer
  Vector2D v = getCenter().distanceVec( m_target->getCenter() );
  
  std::cout<<"proximity "<<proximity << " length "<<v.length();
  if( proximity > v.length() ){
    close_enough = false;
    m_coords += m_speed * v.norm();
  }
  else close_enough = true;

  return *this;
}

Range &Range::attack(std::list<std::unique_ptr<Projectile>> &projectile_list)
{
  if( m_target == nullptr )
    return *this;

  std::cout << "attack cooldown" << m_attack_cooldown_left << std::endl;
  if(close_enough && m_attack_cooldown_left <= 0){
    m_attack_cooldown_left = m_attack_cooldown;
    shoot(projectile_list);
  }
  std::cout << std::endl;
  return *this;
}

Range &Range::shoot(std::list<std::unique_ptr<Projectile>> &bullet_list)
{
  Vector2D shooting_direction = getCenter().distanceVec( m_target->getCenter() );
  bullet_list.merge( m_weapon->fire( getCenter(), shooting_direction , true, false ) );
  
  return *this;
}
