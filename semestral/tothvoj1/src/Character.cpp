#include "Character.hpp"

Character::Character()
  : GameObject(), IMovement(), m_max_lives(0), m_lives(0), m_dmg_cooldown(0), m_dmg_cooldown_left(0)
{
}

Character::Character( const Texture &t, int x, int y, const std::string& tag, double speed, int max_lives, float damage_cooldown)
  : GameObject(t, x, y, tag), IMovement(speed, {0,0}), m_max_lives(max_lives), m_lives(max_lives), 
    m_dmg_cooldown(damage_cooldown), m_dmg_cooldown_left(0)
{
}

Character::~Character()
{
}

bool Character::reduceHealth(int damage)
{
  if( damage > 0 && m_dmg_cooldown_left <= 0 ){
    m_dmg_cooldown_left = m_dmg_cooldown;
    m_lives -= damage;
  }
  return m_lives <= 0 ? false : true;
}

bool Character::increaseHealth(int heal)
{
  bool healed = m_lives == m_max_lives;
  if( heal > 0 ){
    m_lives += heal;
    m_lives = m_lives > m_max_lives ? m_max_lives : m_lives;
  }
  return healed;
}

bool Character::isDead()
{
  return m_lives <= 0;
}

int Character::getHealth()
{
  return m_lives;
}

void Character::adjust(double offset_x, double offset_y)
{
  m_coords = Vector2D( m_coords.x - offset_x, m_coords.y - offset_y );
}

Character& Character::updateTimers(const float &tick)
{
  m_dmg_cooldown_left -= tick;  
  return *this;
}