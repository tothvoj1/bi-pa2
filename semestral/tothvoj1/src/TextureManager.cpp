#include "TextureManager.hpp"

TextureManager::TextureManager()
{
}

TextureManager::~TextureManager()
{
}

bool TextureManager::loadPathsFromJSON(const std::string &path)
{
  std::ifstream json_file(path);
  if( !json_file )
    return false;
  
  loaded_paths = nlohmann::json::parse(json_file);

  if( loaded_paths.is_discarded() )
    return false;
  return true;
}

bool TextureManager::loadTextureByTag(const std::string &tag, SDL_Renderer * renderer)
{
  if( loaded_textures.find(tag) != loaded_textures.end() ){
    std::cout << "eeeeeeeeeeii" << std::endl;
    return false; //already stored
    }
  auto it = loaded_paths.find(tag);
  if( it == loaded_paths.end() ){
    for( auto i : loaded_paths )
      std::cout << i << std::endl;
    std::cout << "hiiiiiiiiiiiiiii" << std::endl;
    return false; //dont know the path
  }


  loaded_textures.insert( {tag, Texture(*it, renderer) } );

  return true;
}

Texture TextureManager::getTexture(const std::string &tag)
{
  auto it = loaded_textures.find(tag);
  if(  it == loaded_textures.end() )
    throw std::runtime_error("Texture for tag "+tag+" not found. Maybe you didn't use loadPathsFromJSON or loadTextureByTag?");
  
  return it->second;
}