#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <memory>
#include <iostream>
#include <queue>
// #include "EventHandler.hpp"
#include "TextureManager.hpp"
#include "Scene.hpp"
#include "MainMenu.hpp"
#include "Level.hpp"
#include "assert.h"

#define TEXTURE_PATH_FILE "assets/config/paths.json"

class Game{
  public:
    Game();
    ~Game();

    Game& init();
    Game& run();

  private:
    //attributes
    bool            running;
    Scene*          m_scene; //current scene

    float next_time = 0;
    const float dt = 1000.0 / 30.0;

    std::unique_ptr<SDL_Window, decltype(&SDL_DestroyWindow)>   m_window;
    
    std::shared_ptr<SDL_Renderer> m_renderer;
    TextureManager  tm;

    //methods
    std::unique_ptr<SDL_Window, decltype(&SDL_DestroyWindow)>   initWindow( int w, int h);
    std::shared_ptr<SDL_Renderer> initRenderer();
};