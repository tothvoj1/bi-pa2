#pragma once

class Box{
  public:
    double x, y, w, h;

    void overlap( const Box& other, double& horizontal, double& vertical ) const;
    bool isColliding( const Box& other ) const;
  protected:
    double intervalOverlap( const Box& other, bool horizontal ) const;
};