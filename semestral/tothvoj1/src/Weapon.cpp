#include "Weapon.hpp"

Weapon::Weapon() : m_projectile(nullptr), reload_time( 0 ), cooldown_left( 0 )
{
}

Weapon::Weapon(const std::shared_ptr<Projectile>& projectile, float reload_time) 
  : m_projectile(projectile), reload_time( reload_time ), cooldown_left( 0 )
{
  
}

Weapon::~Weapon()
{
}

std::list<std::unique_ptr<Projectile>> Weapon::fire(const Vector2D &origin, const Vector2D &direction, bool ignoreEnemy, bool ignorePlayer)
{
  if( cooldown_left > 0 )
    return {};

  cooldown_left = reload_time;

  std::list<std::unique_ptr<Projectile>> list;
  
  auto p = Projectile( *m_projectile );
  p.setCoordsByCenter(origin);
  p.setDirection(direction);
  p.setIgnoreEnemy(ignoreEnemy);
  p.setIgnorePlayer(ignorePlayer);
  list.push_back( std::make_unique<Projectile>(p) );
  return list;
}

Weapon &Weapon::updateCooldown(const float &ticks)
{
  cooldown_left -= ticks;
  return *this;
}
