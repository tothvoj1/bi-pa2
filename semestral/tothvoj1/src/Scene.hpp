#pragma once

#include "SDL2/SDL.h"

/// @brief Abstract class responsible for storing, managing and showing content on window 
class Scene{
  public:
    Scene();
    virtual ~Scene();

    virtual bool    processInput() = 0;
    virtual Scene*  update( const float& tick ) = 0;
    virtual void    render(SDL_Renderer* renderer) = 0;
};