#include "Room.hpp"

Room::Room() : m_tilemap(nullptr)
{
}

Room::~Room()
{
}

Room *Room::update( const float& tick, Player& player, const CollisionHandler& handy )
{
  //update Player
  player.updateTimers( tick );
  player.move();
  
  //check collision with tiles on tilemap
  auto tiles = m_tilemap->getTiles( player.getArea() );
  handy.handle( player, tiles );
  //check collisions with enemies
  for( auto e : m_enemies )
    handy.handle( player, *e );
  if( player.isShooting() )
    player.shoot( m_projectiles );
  
  for( auto e : m_enemies ){
    e->updateTimers( tick );
    e->move();
    e->attack( m_projectiles );
    
    auto tiles = m_tilemap->getTiles( e->getArea() );
    handy.handle( *e, tiles );
    handy.handle( *e, player );
  }

  for( const auto& p : m_projectiles ){
    p.get()->move();

    auto tiles = m_tilemap->getTiles( p->getArea() );
    handy.handle( *p.get(), tiles );
    handy.handle( *p.get(), player );
    for( auto e : m_enemies )
      handy.handle( *p.get(), *e );
  }
  
  // remove dead and broken
  for( auto e_it = m_enemies.begin(); e_it != m_enemies.end(); ){
    if( (*e_it)->isDead() )
      m_enemies.erase( e_it++ );
    else e_it ++;
  }
  for( auto p_it = m_projectiles.begin(); p_it != m_projectiles.end(); ){
    if( (*p_it)->hadImpact() )
      m_projectiles.erase( p_it++ );
    else p_it ++;
  }

  return this;
}

void Room::render( SDL_Renderer* renderer, Player& player )
{
  m_tilemap->render( renderer );
  player.render( renderer );
  for( auto e : m_enemies )
    e->render( renderer );
  for( const auto& p : m_projectiles )
    p.get()->render(renderer);
}

Room *Room::setTileMap(TileMap* tilemap)
{
  m_tilemap = tilemap;
  return this;
}

Room *Room::addEnemy(Enemy *enemy)
{
  m_enemies.push_back(enemy);

  return this;
}

SDL_Rect Room::getDimensions()
{
  return { m_tilemap->getWidth(), 
    m_tilemap->getHeight(), 
    m_tilemap->getWidth(), 
    m_tilemap->getHeight()        };
}
