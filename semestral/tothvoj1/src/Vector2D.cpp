#include "Vector2D.hpp"
#include <iostream>

Vector2D::Vector2D()
  : x(0), y(0)
{
}

Vector2D::Vector2D(const Vector2D &other)
  :x(other.x), y(other.y)
{
}

Vector2D::Vector2D(double x, double y)
  : x(x), y(y)
{
}

Vector2D::Vector2D(int x, int y)
  : x(double(x)), y(double(y))
{
}

double Vector2D::length() const
{
  return sqrt( x * x + y * y );
}

Vector2D Vector2D::operator+(const Vector2D &other) const
{
  return Vector2D( x + other.x, y + other.y );
}

Vector2D Vector2D::operator-(const Vector2D &other) const
{
  return Vector2D( x - other.x, y - other.y );
}

Vector2D &Vector2D::operator+=(const Vector2D &other)
{
  *this = (*this) + other;
  return *this;
}

Vector2D &Vector2D::operator-=(const Vector2D &other)
{
  *this = (*this) - other;
  return (*this);
}

bool Vector2D::operator==(const Vector2D &other) const
{
  double epsilon = std::numeric_limits<double>::epsilon();
  return std::fabs(x - other.x) < epsilon;
}

Vector2D Vector2D::norm() const
{
  auto len = length();

  if( std::isnan(len) || len == 0 )
    return Vector2D(0, 0);

  auto norm_x = x / len;
  auto norm_y = y / len;

  return Vector2D(norm_x, norm_y);
}

Vector2D Vector2D::distanceVec(const Vector2D &other) const
{
  return other - *this;
}

std::ostream &operator<<(std::ostream &os, const Vector2D &v)
{
  os << "[" << v.x << "," << v.y << "]";
  return os;
}

Vector2D operator*(const double &scalar, const Vector2D &other)
{
  return Vector2D(scalar * other.x, scalar * other.y);
}

Vector2D operator*(const int &scalar, const Vector2D &other)
{
  return Vector2D(scalar * other.x, scalar * other.y);
}

// // #define TEST
// #ifndef TEST
// #include <assert.h>
// void test(){
//   assert( == )
// }
// #endif