#pragma once

#include "GameObject.hpp"
#include "IMovement.hpp"

/// @brief Abstract class for any living breathing entity in the game. Game character can be player or any type of enemy. It's an Object, so it has its coords in the world, but can also move and die
class Character : public GameObject, public IMovement
{
  public:
    Character();
    Character( const Texture& t, int x, int y, const std::string& tag, double speed, int max_lives, float damage_cooldown = 1.5 * 1000);
    virtual ~Character();
    
    /// @brief Reduce characters health by damage, does nothing if damage is 0 or negative
    /// @param damage - positive integer
    /// @return true if characters health is above 0 (survived), false if equal or below 0 (death)
    bool reduceHealth( int damage );
    
    /// @brief Increase characters health by heal, does nothing if heal is ) or negative
    /// @param heal - positive integer
    /// @return true if characters health can be increased (is not maxed), zero if can't
    bool increaseHealth( int heal );
    
    bool isDead();

    int getHealth();

    virtual void adjust(double offset_x, double offset_y);
  
    virtual Character& updateTimers( const float& tick );

  protected:
    int     m_max_lives;
    int     m_lives;
    float   m_dmg_cooldown;
    float   m_dmg_cooldown_left;
};