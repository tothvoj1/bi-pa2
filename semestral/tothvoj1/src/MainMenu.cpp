#include "MainMenu.hpp"

MainMenu::MainMenu()
  : m_selected(0), m_next_choice(0), m_next_scene(false)
{
}

MainMenu::~MainMenu()
{
}

bool MainMenu::processInput()
{
  SDL_Event e;
  
  while (SDL_PollEvent(&e)) {
    if (e.type == SDL_QUIT) {
      return false;
    }
    else if( e.type == SDL_KEYDOWN && e.key.repeat == 0 ){
      // std::cout << "key down" << std::endl;
      auto code = e.key.keysym.sym;
      if( code == SDLK_UP )        
        m_next_choice += -1;
      else if( code == SDLK_DOWN )        
        m_next_choice += 1;    
    }
    else if( e.type == SDL_KEYUP && e.key.repeat == 0 ){
      auto code = e.key.keysym.sym;
      if( code == SDLK_UP )        
        m_next_choice += 1;
      else if( code == SDLK_DOWN )        
        m_next_choice += -1;    
      else if( code == SDLK_e )        
        m_next_scene = true;    
    }
  }
  return true;
}

Scene *MainMenu::update( const float& tick )
{
  if( !m_buttons.empty() && m_next_choice != 0 ){
    if( m_next_time < SDL_GetTicks() )  {
      m_next_time = SDL_GetTicks() + m_button_reload;
      
      m_selected += m_next_choice;
      m_selected = m_selected < 0 
        ? m_buttons.size() - 1
        : m_selected % m_buttons.size();
    
      auto coords1 = m_underscore->getCoords();
      auto coords2 = m_buttons[m_selected]->getCoords();
      m_underscore->setCoords({coords1.x,coords2.y});
    }
  }
  
  if( m_next_scene ){
    m_next_scene = false;
    return m_scenes[m_selected];
  }  
  return this;
}

void MainMenu::render( SDL_Renderer* renderer )
{
  SDL_SetRenderDrawColor(renderer, 255, 0, 0 , 255);
  SDL_RenderClear(renderer);

  int count = m_buttons.size();
  for( int i = 0; i < count; i++ ){
    if( i == m_selected )
      m_underscore->render(renderer);
    m_buttons[i]->render(renderer);
  }
  SDL_RenderPresent(renderer);
}

MainMenu& MainMenu::addButton(GameObject* button, Scene *scene)
{
  m_buttons.push_back(button);
  m_scenes.push_back(scene);
  
  int w, h;
  auto dm = SDL_DisplayMode();
  SDL_GetCurrentDisplayMode(0, &dm);
  w = dm.w;
  h = dm.h;
  // SDL_GetRendererOutputSize(renderer,&w,&h);

  int total_h = 0;
  for( const auto& b : m_buttons )
    total_h += b->getTexture().getHeight();

  auto y = h / 2 - total_h / 2;
  int offset = 0;
  for( auto b : m_buttons ){
    auto rect = b->getCoords();
    b->setCoords({rect.x, double(y + offset)});  
    offset += b->getTexture().getHeight();
  }

  if(m_underscore != nullptr)
    m_underscore->setCoords( m_buttons[0]->getCoords() );


  return *this;
}

MainMenu& MainMenu::setUnderscore(GameObject *underscore)
{
  m_underscore = underscore;
  if(!m_buttons.empty())
    m_underscore->setCoords( m_buttons[0]->getCoords() );

  return *this;
}