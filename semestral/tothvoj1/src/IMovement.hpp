#pragma once

#include "Vector2D.hpp"

class IMovement
{
  public:
    IMovement();
    IMovement(int speed, const Vector2D& direction);
    virtual ~IMovement();

    virtual IMovement& move() = 0;
    virtual void adjust(double offset_x, double offset_y) = 0;

    IMovement& setSpeed(double speed);
    IMovement& setDirection(const Vector2D& direction);
  protected:
    double    m_speed;
    Vector2D  m_direction;
};