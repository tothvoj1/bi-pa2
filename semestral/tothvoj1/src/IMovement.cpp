#include "IMovement.hpp"

IMovement::IMovement()
  : m_speed(0)
{
}

IMovement::IMovement(int speed,  const Vector2D& direction)
  : m_speed(speed), m_direction(direction)
{
}

IMovement::~IMovement()
{
}

IMovement &IMovement::setSpeed(double speed)
{
  if( speed >= 0 )
    m_speed = speed;
  return *this;
}

IMovement &IMovement::setDirection(const Vector2D &direction)
{
  m_direction = direction;
  return *this;
}
