#include "Game.hpp"

Game::Game()
  :  running(true), m_scene(nullptr),
  m_window(std::unique_ptr<SDL_Window, decltype(&SDL_DestroyWindow)>(nullptr, SDL_DestroyWindow)),
  m_renderer(std::shared_ptr<SDL_Renderer>(nullptr))
{
  if( SDL_Init( SDL_INIT_EVERYTHING ) < 0 )
    throw std::logic_error( "Error while initing SDL2: " + std::string(SDL_GetError()) );
  
  if( IMG_Init( IMG_INIT_PNG ) < 0 )
    throw std::logic_error( "Error while initing SDL2: " + std::string(SDL_GetError()) );
  
  if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
    std::cerr << "Warning: Linear texture filtering not enabled!" << std::endl;

  m_window = std::move( initWindow(720, 480) );
  m_renderer = initRenderer();
  //manager = AssetManager(m_renderer);
}

Game::~Game()
{
  IMG_Quit();
  SDL_Quit();
}

Game& Game::init()
{
  assert(tm.loadPathsFromJSON("assets/config/paths.json"));

  assert(tm.loadTextureByTag("btn_continue", m_renderer.get()));
  assert(tm.loadTextureByTag("btn_new_game", m_renderer.get()));
  assert(tm.loadTextureByTag("btn_quit"    , m_renderer.get()));
  assert(tm.loadTextureByTag("underscore"  , m_renderer.get()));
  assert(tm.loadTextureByTag("player"      , m_renderer.get()));
  assert(tm.loadTextureByTag("path"        , m_renderer.get()));
  assert(tm.loadTextureByTag("spike"       , m_renderer.get()));
  assert(tm.loadTextureByTag("wall"        , m_renderer.get()));
  assert(tm.loadTextureByTag("door"        , m_renderer.get()));
  assert(tm.loadTextureByTag("melee"        , m_renderer.get()));
  assert(tm.loadTextureByTag("range"        , m_renderer.get()));
  assert(tm.loadTextureByTag("bullet"        , m_renderer.get()));

  auto txtr1 = tm.getTexture("btn_continue" );
  auto txtr2 = tm.getTexture("btn_new_game" );
  auto txtr3 = tm.getTexture("btn_quit"     );
  auto txtr4 = tm.getTexture("underscore"   );
  auto txtr5 = tm.getTexture("player"       );
  auto txtr6 = tm.getTexture("path"         );
  auto txtr7 = tm.getTexture("spike"        );
  auto txtr8 = tm.getTexture("wall"         );
  auto txtr9 = tm.getTexture("door"         );
  auto txtr10 = tm.getTexture("melee"         );
  auto txtr11 = tm.getTexture("range"         );
  auto txtr12 = tm.getTexture("bullet"         );

  GameObject* btn_continue = new GameObject(txtr1, 0,0,"btn_continue");
  GameObject* btn_new_game = new GameObject(txtr2, 0,0,"btn_new_game");
  GameObject* btn_quit = new GameObject(    txtr3, 0,0, "btn_quit"    );
  GameObject* underscore = new GameObject(    txtr4, 0,0, "underscore"    );
  
  auto bullet = std::shared_ptr<Projectile>(new Projectile( txtr12, 0,0, "bullet", 3, {0,0} , 1));
  
  auto player = std::shared_ptr<Player>(new Player(txtr5, 320, 33, 6, 4, std::unique_ptr<Weapon>(new Weapon(bullet, 700))));

  Enemy* melee = new Melee(txtr10, 33, 33, 3, 3, 2, 1, 1);
  Enemy* range = new Range(txtr10, 129, 150, 3, 3, 65, 1, 1, std::unique_ptr<Weapon>(new Weapon(bullet, 700)));


  melee->setTarget(player);
  range->setTarget(player);

  // wall = 0
  // path = 1
  // door = 2
  // spikes = 3
  std::vector<Texture> tile_textures = {
    txtr8, txtr6, txtr9, txtr7
  };
  //this will be loaded from safe file or generated and then saved
  std::vector<std::vector<int>> tiles = {
    { 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0 },
    { 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0 },
    { 0, 1, 3, 3, 3, 1, 1, 1, 3, 3, 3, 1, 0 },
    { 0, 1, 3, 1, 1, 1, 3, 1, 1, 1, 3, 1, 0 },
    { 2, 1, 3, 1, 1, 3, 1, 3, 1, 1, 3, 1, 2 },
    { 0, 1, 3, 1, 1, 1, 3, 1, 1, 1, 3, 1, 0 },
    { 0, 1, 3, 3, 3, 1, 1, 1, 3, 3, 3, 1, 0 },
    { 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0 },
    { 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0 }
  };

  TileMap* tilemap = new TileMap(tile_textures, tiles);

  MainMenu* menu  = new MainMenu( );
  // MainMenu* pick_save  = new MainMenu( );
  Level* level = new Level();

  Room *room = new Room();

  
  room->setTileMap( tilemap )
      ->addEnemy( melee )
      ->addEnemy( range );

  level->setPlayer( player )
      ->setRoom( room );

  menu->setUnderscore(underscore)
      .addButton( btn_continue, level )
      .addButton( btn_new_game, level )
      .addButton( btn_quit, menu );

  m_scene = menu;

  // running = false;

  return *this;
}

Game& Game::run()
{
  while( running ){
    if( SDL_GetTicks() > next_time ){
      next_time = SDL_GetTicks() + dt;

      m_scene->render(m_renderer.get());
      running = m_scene->processInput();
      m_scene = m_scene->update( dt );
    }
  }

  return *this;
}

std::unique_ptr<SDL_Window, decltype(&SDL_DestroyWindow)> Game::initWindow(int w, int h)
{
  auto window = SDL_CreateWindow( "Adalberts Epiphany", 
                                SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 
                                w, h, SDL_WINDOW_SHOWN );
  if( window == NULL )
    throw std::logic_error( "Error while creating window: " + std::string(SDL_GetError()) );

  return std::unique_ptr<SDL_Window, decltype(&SDL_DestroyWindow)> (window, SDL_DestroyWindow);
}

std::shared_ptr<SDL_Renderer> Game::initRenderer()
{
  auto renderer = SDL_CreateRenderer( m_window.get(), -1, SDL_RENDERER_ACCELERATED );
  
  if( renderer == NULL )
    throw std::logic_error( "Error while creating renderer: " + std::string(SDL_GetError()) );
	
  return std::shared_ptr<SDL_Renderer> (renderer, SDL_DestroyRenderer);
}