#pragma once

#include "GameObject.hpp"

class Tile : public GameObject{
  public:
    Tile();
    Tile( const Texture &t, int x, int y, const std::string& tag);
    virtual ~Tile();

    virtual bool isSafe() const;
    virtual bool isWalkable() const;
    virtual bool isPlayerOnly() const;
};

class Path : public Tile{
  public: 
    Path( const Texture &t, int x, int y) : Tile(t,x,y,"path") {}
    bool isWalkable() const { return true; }
    bool isSafe() const { return true; }
    bool isPlayerOnly() const { return false; };
};

class Wall : public Tile{
  public:
    Wall( const Texture &t, int x, int y) : Tile(t,x,y,"wall") {}
    bool isWalkable() const { return false; }
    bool isSafe() const { return true; }
    bool isPlayerOnly() const { return false; };
  private:
};

class Door : public Tile{
  public:
    Door( const Texture &t, int x, int y ) : Tile(t,x,y,"door") {}
    bool isWalkable() const { return true; }
    bool isSafe() const { return true; }
    bool isPlayerOnly() const { return true; };
};

class Spikes : public Tile{
  public:
    Spikes( const Texture &t, int x, int y) : Tile(t,x,y,"spikes") {}
    bool isWalkable() const { return true; }
    bool isSafe() const { return false; }
    bool isPlayerOnly() const { return false; };
};