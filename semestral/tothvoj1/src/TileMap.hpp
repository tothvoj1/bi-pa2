#pragma once

#include <list>
#include <vector>
#include <memory>
#include "Tile.hpp"
#include "ETiles.hpp"

/// @brief This characters represent where the characters, items and other objects live. 
/// It also divides the map in such a way searching algorithm can be applied for situations
/// like enemy chasing player
class TileMap{
  public:
    TileMap();
    TileMap(std::vector<Texture> textures, std::vector<std::vector<int>> tiles);
    ~TileMap() = default;
    
    void render( SDL_Renderer* renderer );

    /// @brief Use this method to get every tile intersected by rectangle madde from parameters
    /// @param coords upper left corner according to the tileMap (not the window)
    /// @param width 
    /// @param height 
    /// @return list of pointers to tiles
    std::list<std::shared_ptr<Tile>> getTiles( const Box& box );

    /// @brief Computes width as width of a single tile times number of columns
    /// @return Total width in pixels
    int getWidth();

    /// @brief Computes height as height of a single tile times number of rows
    /// @return Total height in pixels
    int getHeight();
  
  private:
    /// @brief tiles are stored in matrice of pointers as vector of rows, Coords coordinates {x,y}
    /// can be mapped to tile on index m_tiles[ row(y) ][ col(x) ]
    std::vector<std::vector<std::shared_ptr<Tile>>> m_tiles;

    int tile_w;
    int tile_h;

    void setTilesCoordinates();
    bool intersect( const Tile& t, const Box& box);
};