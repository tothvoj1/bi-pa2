#include "Level.hpp"

Level::Level()
  : m_key_to_command( {
    SDLK_a,
    SDLK_d,
    SDLK_w,
    SDLK_s,
    SDLK_LEFT,
    SDLK_RIGHT,
    SDLK_UP,
    SDLK_DOWN,
    SDLK_e} ), m_adalbert(nullptr), m_room(nullptr)
{
  
}

Level::~Level()
{
}

bool Level::processInput()
{
  SDL_Event e;
  
  while (SDL_PollEvent(&e)) {
    if (e.type == SDL_QUIT) {
      return false;
    }
    else if( e.type == SDL_KEYDOWN && e.key.repeat == 0 ){
      // std::cout << "key down" << std::endl;
      auto code = e.key.keysym.sym;
      if( code == m_key_to_command[ SHOOT_LEFT  ] )        
        m_adalbert->beginShoot( {-1,0} );
      else if( code == m_key_to_command[ SHOOT_RIGHT ] )        
        m_adalbert->beginShoot( { 1,0} );      
      else if( code == m_key_to_command[ SHOOT_UP    ] )        
        m_adalbert->beginShoot( {0,-1} );      
      else if( code == m_key_to_command[ SHOOT_DOWN  ] )        
        m_adalbert->beginShoot( {0, 1} );    
      else if( code == m_key_to_command[ MOVE_LEFT   ] )        
        m_adalbert->step(LEFT);    
      else if( code == m_key_to_command[ MOVE_RIGHT  ] )        
        m_adalbert->step(RIGHT);    
      else if( code == m_key_to_command[ MOVE_UP     ] )        
        m_adalbert->step(UP);    
      else if( code == m_key_to_command[ MOVE_DOWN   ] )        
        m_adalbert->step(DOWN);     
      else if( code == m_key_to_command[ DROP_BOMB   ] )        
        m_adalbert->step(LEFT);     
    }
    //If a key was released
    else if( e.type == SDL_KEYUP && e.key.repeat == 0 ){
      auto code = e.key.keysym.sym;
      if( code == m_key_to_command[ SHOOT_LEFT  ] )        
        m_adalbert->endShoot( {-1,0} );
      else if( code == m_key_to_command[ SHOOT_RIGHT ] )        
        m_adalbert->endShoot( { 1,0} );      
      else if( code == m_key_to_command[ SHOOT_UP    ] )        
        m_adalbert->endShoot( {0,-1} );      
      else if( code == m_key_to_command[ SHOOT_DOWN  ] )        
        m_adalbert->endShoot( {0, 1} );    
      else if( code == m_key_to_command[ MOVE_LEFT   ] )        
        m_adalbert->step(RIGHT);    
      else if( code == m_key_to_command[ MOVE_RIGHT  ] )        
        m_adalbert->step(LEFT);    
      else if( code == m_key_to_command[ MOVE_UP     ] )        
        m_adalbert->step(DOWN);    
      else if( code == m_key_to_command[ MOVE_DOWN   ] )        
        m_adalbert->step(UP);     
      else if( code == m_key_to_command[ DROP_BOMB   ] )        
        m_adalbert->step(RIGHT);     
    }
  }

  return true;
}

Scene *Level::update( const float& tick )
{
  m_room = m_room->update(tick, *m_adalbert, m_handler);

  return this;
}

void Level::render( SDL_Renderer* renderer )
{
  SDL_SetRenderDrawColor(renderer, 178, 26, 200 , 255);

  SDL_Rect tmp_rect, old_rect;
  SDL_RenderGetViewport( renderer, &old_rect);
  
  tmp_rect = m_room->getDimensions();
  tmp_rect.x = old_rect.w / 2 - tmp_rect.x / 2;
  tmp_rect.y = old_rect.h / 2 - tmp_rect.y / 2;
  
  SDL_RenderSetViewport( renderer, &tmp_rect);
  SDL_RenderClear(renderer);
  
  m_room->render( renderer, *m_adalbert );

  SDL_RenderPresent( renderer );
  SDL_RenderSetViewport( renderer, &old_rect);
}

Level *Level::setPlayer(std::shared_ptr<Player>& player)
{
  //TODO: if player was set, what happens to it?
  m_adalbert = player;

  return this;
}

Level *Level::setRoom(Room *room)
{
  //TODO: if room was set, what happens to it?
  m_room = room;
  return this;
}
