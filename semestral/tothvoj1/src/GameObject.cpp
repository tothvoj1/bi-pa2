#include "GameObject.hpp"

GameObject::GameObject()
{
}

GameObject::GameObject( const Texture& t, int x, int y, const std::string& tag)
  : m_texture(t), m_coords({x,y}), m_tag(tag)
{
}

GameObject::~GameObject()
{
}

void GameObject::render(SDL_Renderer * r, int offset_x, int offset_y)
{
  // std::cout << m_tag<< " " <<(int)m_coords.x<<" " << (int)m_coords.y<<", "<<m_coords.x<<" " << m_coords.y<< std::endl;
  m_texture.render( (int)m_coords.x + offset_x, (int)m_coords.y + offset_x, r );
}

std::string GameObject::getTag() const
{
  return m_tag;
}

const Texture& GameObject::getTexture() const
{
  return m_texture;
}

Vector2D GameObject::getCoords() const
{
  return m_coords;
}

GameObject& GameObject::setCoords(const Vector2D &coords)
{
  m_coords = coords;
  return *this;
}

GameObject& GameObject::setCoordsByCenter(const Vector2D &center)
{
  Vector2D offset = { m_texture.getWidth()/2, m_texture.getHeight()/2 };
  m_coords = center - offset;
  return *this;
}

Box GameObject::getArea() const
{
  return { m_coords.x, m_coords.y, double(m_texture.getWidth()), double(m_texture.getHeight()) };
}

Vector2D GameObject::getCenter() const
{
  return { m_coords.x + m_texture.getWidth() / 2 , m_coords.y + m_texture.getHeight() / 2 };
}

// GameObject &GameObject::setTexturePos(const int& x, const int& y)
// {
//   m_coords.x = x;
//   m_coords.y = y;
// }

// SDL_Rect GameObject::getTextureRect()
// {
//   return m_texture_rect;
// }
