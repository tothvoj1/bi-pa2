#pragma once

#include "Texture.hpp"
#include "Vector2D.hpp"
#include "Box.hpp"
#include <SDL2/SDL.h>
#include <iostream>
#include <string>

/// @brief Base class for whatever needs to be shown on screen
class GameObject
{
  public:
    GameObject();
    GameObject( const Texture& t, int x, int y, const std::string& tag);
    virtual ~GameObject();

    void render(SDL_Renderer * r, int offset_x = 0, int offset_y = 0) ;

    std::string getTag() const;

    const Texture& getTexture() const;
    // GameObject& setTexturePos(const int& x, const int& y );
    // SDL_Rect    getTextur  eRect( );
    Vector2D        getCoords() const;
    GameObject&     setCoords( const Vector2D& coords );
    GameObject&     setCoordsByCenter( const Vector2D& center );

    Box       getArea() const;
    Vector2D  getCenter() const;
  protected:
    Texture  m_texture;
    Vector2D      m_coords;
    std::string   m_tag;
};