#pragma once

#include "GameObject.hpp"
#include "IMovement.hpp"
#include "Vector2D.hpp"
#include <memory>

class Projectile : public GameObject, public IMovement{
  public:
    Projectile();
    Projectile( const Texture& t, int x, int y, const std::string& tag, int speed, const Vector2D& direction, int damage);
    virtual ~Projectile();
    Projectile&  move();
    virtual void adjust(double offset_x, double offset_y);
    

    Projectile& setIgnorePlayer(bool hurt);
    Projectile& setIgnoreEnemy(bool hurt);
    Projectile& Impact();
    
    bool        ignorePlayer();
    bool        ignoreEnemy();
    bool hadImpact();

    int         getDamage();
  protected:
    int         m_damage;
    
    bool        m_hit_something;
    
    bool        m_ignore_player;
    bool        m_ignore_enemy;
};
