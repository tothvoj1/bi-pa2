#pragma once

enum Direction {
  LEFT = 90,
  RIGHT = 270,
  UP = 0,
  DOWN = 180
};