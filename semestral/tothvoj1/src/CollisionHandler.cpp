#include "CollisionHandler.hpp"

CollisionHandler::CollisionHandler()
{
}

CollisionHandler::~CollisionHandler()
{
}

void CollisionHandler::pushAway( Character &ch, const GameObject &ob ) const
{
    if ( ch.getArea().isColliding( ob.getArea() ) ){ 
    double x_over, y_over;

    ch.getArea().overlap( ob.getArea(), x_over, y_over );
    
    double absx = x_over < 0 ? - x_over : x_over;
    double absy = y_over < 0 ? - y_over : y_over;

    if( absx < absy ){
      ch.adjust( x_over , 0);
      if( ch.getArea().isColliding( ob.getArea() ) )
        ch.adjust( -x_over , y_over);
      if( ch.getArea().isColliding( ob.getArea() ) )
        ch.adjust( x_over , 0);
    }
    else{
      ch.adjust( 0 , y_over);
      if( ch.getArea().isColliding( ob.getArea() ) )
        ch.adjust( x_over , -y_over);
      if( ch.getArea().isColliding( ob.getArea() ) )
        ch.adjust( 0 , y_over);
    }
  }
}

void CollisionHandler::pushAway(Projectile &p, const GameObject &ob)  const
{
   if ( p.getArea().isColliding( ob.getArea() ) ){ 
    double x_over, y_over;

    p.getArea().overlap( ob.getArea(), x_over, y_over );
    
    double absx = x_over < 0 ? - x_over : x_over;
    double absy = y_over < 0 ? - y_over : y_over;

    if( absx < absy ){
      p.adjust( x_over , 0);
      if( p.getArea().isColliding( ob.getArea() ) )
        p.adjust( -x_over , y_over);
      if( p.getArea().isColliding( ob.getArea() ) )
        p.adjust( x_over , 0);
    }
    else{
      p.adjust( 0 , y_over);
      if( p.getArea().isColliding( ob.getArea() ) )
        p.adjust( x_over , -y_over);
      if( p.getArea().isColliding( ob.getArea() ) )
        p.adjust( 0 , y_over);
    }
  }
}

void CollisionHandler::handle( Player &p, const std::list<std::shared_ptr<Tile>>  &tiles)  const
{
  for( auto tile : tiles){
    if( !tile->isSafe() )
      p.reduceHealth(1);
    if( !tile->isWalkable() )
      pushAway( p, *tile );
  }
}

void CollisionHandler::handle(Enemy &e, const std::list<std::shared_ptr<Tile>>  &tiles)  const
{
  for( auto tile : tiles){
    if( !tile->isSafe() )
      e.reduceHealth(1);
    if( !tile->isWalkable() || tile->isPlayerOnly())
      pushAway( e, *tile );
    }
}

void CollisionHandler::handle(Projectile &p, const std::list<std::shared_ptr<Tile>>  &tiles)  const
{
  for( auto tile : tiles){
    if( !tile->isWalkable() || tile->isPlayerOnly() ){
      pushAway( p, *tile );
      p.Impact();
    }
  }
}

void CollisionHandler::handle(Character &ch1, Character &ch2)  const
{
  if( !ch1.getArea().isColliding( ch2.getArea() ) )
    return;
  pushAway( ch1, ch2);
}

void CollisionHandler::handle(Projectile &p, Player &c)  const
{
  if( !p.getArea().isColliding( c.getArea() ) )
    return;
  if( p.ignorePlayer() )
    return;

  c.reduceHealth( p.getDamage() );
  p.Impact();
}

void CollisionHandler::handle(Projectile &p, Enemy &c)  const
{
  if( p.ignoreEnemy() || !p.getArea().isColliding( c.getArea() ) )
    return;
  
  c.reduceHealth( p.getDamage() );
  p.Impact();
}
