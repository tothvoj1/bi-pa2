#pragma once

enum class ETiles : int{
  WALL,
  PATH,
  DOOR,
  SPIKES,
  HOLE
};