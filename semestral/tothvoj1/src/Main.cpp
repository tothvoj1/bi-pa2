#include "Game.hpp"

int main( int argc, char* args[] )
{
  Game g;
  
  g.init();
  g.run();
}