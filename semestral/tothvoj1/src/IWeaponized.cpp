#include "IWeaponized.hpp"

IWeaponized::IWeaponized() : m_weapon(nullptr)
{
}

IWeaponized::IWeaponized(std::unique_ptr<Weapon> weapon) : m_weapon(std::move(weapon))
{
}

IWeaponized::IWeaponized(const IWeaponized &other)
  : m_weapon( new Weapon(*other.m_weapon) )
{
}

IWeaponized::IWeaponized(IWeaponized &&other)
  : m_weapon( std::move(other.m_weapon) )
{
}

IWeaponized::~IWeaponized()
{
}
