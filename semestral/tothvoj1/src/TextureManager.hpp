#pragma once

#include "../libs/json.hpp"
#include "Texture.hpp"
#include <iostream>
#include <memory>
#include <unordered_map>
#include <fstream>
#include <string>

/// @brief Loads, stores and provides Texture instances. After loading JSON file associating
/// tags with paths, you can load texture by its tag with loadTextureByTag. The texture will
/// be stored and can be accessed by the tag it was loaded with by using getTexture.
class TextureManager{
  public:
    TextureManager();
    ~TextureManager();

    /// @brief Loads and stores paths to all textures specified in path. Textures can then be loaded with 'loadTextureByTag' function
    /// @param path path to the JSON file containing tag : paths to textures 
    /// @return false on error - file cannot be opened or json didn't parse, true if nothing went wrong
    bool loadPathsFromJSON( const std::string& path );

    /// @brief After loading all paths with loadPathsFromJSON, individual textures can be loaded with
    /// this function. 
    /// @param tag string used to find the path to the texture
    /// @return true if Texture was loaded succesfuly, false on error
    bool loadTextureByTag(const std::string &tag, SDL_Renderer * renderer);

    /// @brief Getter for SDL_Texture stored in file specified by tag. Use loadPathsFromJSON 
    /// and loadTextureByTag before using this function
    /// @param tag - string representing the tag of the object using this texture
    /// @throws std::runtime_error when texture loading fails
    /// @return Texture instance on succes, throws runtime_error otherwise
    Texture getTexture( const std::string& tag );
  private:
    /// @brief paths to the texture files looooooaded with loadPathsFromJSON
    nlohmann::json loaded_paths;
    /// @brief textures loaded with loadTextureByTag
    std::unordered_map<std::string, Texture> loaded_textures;
};