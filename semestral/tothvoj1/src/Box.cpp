#include "Box.hpp"

void Box::overlap( const Box& other, double& horizontal, double& vertical ) const
{
  horizontal = intervalOverlap(other, true);
  vertical = intervalOverlap(other, false);
}

bool Box::isColliding( const Box &other) const
{
  return  x < other.x + other.w &&
    x + w > other.x &&
    y < other.y + other.h &&
    h + y > other.y;
}

double Box::intervalOverlap(const Box &other, bool horizontal ) const
{
  double l1 = horizontal ?       x :       y;
  double r1 = horizontal ?       w :       h;
  double l2 = horizontal ? other.x : other.y;
  double r2 = horizontal ? other.w : other.h;

  if( l1 <= l2 && l2 <= l1 + r1 ) 
    return l1 + r1 - l2;
  else if( l2 <= l1 && l1 <= l2 + r2 ) 
    return l1 - ( l2 + r2 );
  return 0;
}
