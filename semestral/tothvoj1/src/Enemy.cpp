#include "Enemy.hpp"

Enemy::Enemy()
  : Character(), 
    m_attack_range(0),
    m_attack_cooldown(0), 
    m_attack_cooldown_left(0),
    m_attack_damage(0), 
    m_target(nullptr)
{
}

Enemy::Enemy(const Texture &t, int x, int y, const std::string& tag, double speed, int max_lives, double attack_range, 
          int attack_cooldown, int attack_damage)
  : Character( t,x,y,tag,speed,max_lives, 1000), 
    m_attack_range(attack_range), 
    m_attack_cooldown(attack_cooldown), 
    m_attack_cooldown_left(attack_cooldown),
    m_attack_damage(attack_damage), 
    m_target(nullptr)
{
}

Enemy::~Enemy()
{
}

Enemy &Enemy::setTarget(const std::shared_ptr<Character>& target)
{
  m_target = target;
  return *this;
}

int Enemy::getDamage()
{
  return m_attack_damage;
}