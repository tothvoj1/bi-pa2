#pragma once

#include <unordered_map>
#include <vector>
#include "TileMap.hpp"
#include "Enemies.hpp"
#include "Projectile.hpp"
#include "CollisionHandler.hpp"

class Room{
  public:
    Room();
    ~Room();
    
    Room* update( const float& tick, Player& player, const CollisionHandler& handy );
    void  render( SDL_Renderer* renderer, Player& player );

    Room* setTileMap( TileMap* tilemap );
    Room* addEnemy( Enemy * enemy );

    SDL_Rect getDimensions();
  private:
    typedef std::unordered_map<double, std::unordered_map<double, Room*> > RoomMap;
    
    RoomMap m_neighbour_rooms;  //x -> y -> room pointer
    
    TileMap*                  m_tilemap;
    std::list<Enemy*>         m_enemies;
    std::list<std::unique_ptr<Projectile>>    m_projectiles;
};