#pragma once

#include "TileMap.hpp"
#include "Player.hpp"
#include "Enemy.hpp"

/// @brief Class taking care of all collision across all objects in the game.
class CollisionHandler{
  public:
    CollisionHandler();
    ~CollisionHandler();

    void pushAway(  Character &ch, const GameObject &ob  )                          const;
    void pushAway(  Projectile &p, const GameObject &ob  )                          const;

    void handle( Player& p      , const std::list<std::shared_ptr<Tile>> & tiles )  const;
    void handle( Enemy& e       , const std::list<std::shared_ptr<Tile>> & tiles )  const;
    void handle( Projectile& p  , const std::list<std::shared_ptr<Tile>> & tiles )  const;
    
    void handle( Character& ch1 , Character& ch2  )                                 const;
    // void handle( Player& p      , Enemy& e        )                                 const;
    // void handle( Enemy& e       , Player& p       )                                 const;
    void handle( Projectile& p  , Player &c       )                                 const;
    void handle( Projectile& p  , Enemy &c        )                                 const;
};