#include "Projectile.hpp"

Projectile::Projectile()
  : GameObject(), IMovement(), m_damage(0), m_hit_something(false), m_ignore_player(false), m_ignore_enemy(false)
{
}

Projectile::Projectile( const Texture& t, int x, int y, const std::string& tag, int speed, const Vector2D& direction, int damage)
  : GameObject(t,x,y,"bullet"), IMovement(speed, direction), m_damage(damage), m_hit_something(false),
    m_ignore_player(false), m_ignore_enemy(false)
{
}

Projectile::~Projectile()
{
}

Projectile& Projectile::move()
{
  m_coords += m_speed * m_direction.norm();
  return *this;
}

void Projectile::adjust(double offset_x, double offset_y)
{
  m_coords = Vector2D( m_coords.x - offset_x, m_coords.y - offset_y );
}

Projectile &Projectile::setIgnorePlayer(bool hurt)
{
  m_ignore_player = hurt;
  return *this;
}

Projectile &Projectile::setIgnoreEnemy(bool hurt)
{
  m_ignore_enemy = hurt;
  return *this;
}

Projectile& Projectile::Impact()
{
  m_hit_something = true;
  return *this;
}

bool Projectile::ignorePlayer()
{
  return m_ignore_player;
}

bool Projectile::ignoreEnemy()
{
  return m_ignore_enemy;
}

int Projectile::getDamage()
{
  return m_damage;
}

bool Projectile::hadImpact()
{
  return m_hit_something;
}