#pragma once

#include "Enemy.hpp"
#include "IWeaponized.hpp"
#include <cstdlib>

/// @brief You would not believe youe eyes, if ten tousand fireflies
class Range : public Enemy, public IWeaponized{
  public:
    Range();

    Range(const Texture &t, int x, int y, double speed, int max_lives, double attack_range, 
          int attack_cooldown, int attack_damage, std::unique_ptr<Weapon> weapon );

    ~Range() = default;
    
    virtual Range& updateTimers(const float& tick ) override;

    virtual Range& move();
    
    virtual Range& attack(std::list<std::unique_ptr<Projectile>> &projectile_list);

    virtual Range& shoot( std::list<std::unique_ptr<Projectile>>& bullet_list );
  protected:
    bool close_enough;
    bool walk_countdown;
    bool walk_countdown_left;
};
