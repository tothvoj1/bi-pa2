#pragma once

#include "Enemy.hpp"

/// @brief Standart enemy class. Dumbly follows its target. When it is close enough, 
/// deals damage on touch (done in CollisionHandler)
class Melee : public Enemy{
  public:
    Melee();
    Melee(const Texture &t, int x, int y, double speed, int max_lives, double attack_range, 
          int attack_cooldown, int attack_damage);
    
    virtual Enemy& attack( std::list<std::unique_ptr<Projectile>>& projectile_list);
    
    virtual Melee& move();
  
  protected:
};