#include "TileMap.hpp"

TileMap::TileMap()
  : tile_w(0), tile_h(0)
{
}

TileMap::TileMap(std::vector<Texture> textures, std::vector<std::vector<int>> tiles)
{
  // TODO: check width, heigh, row count, 
  tile_h = textures[0].getHeight();
  tile_w = textures[0].getWidth();

  for( const auto& row : tiles ){
    
    std::vector<std::shared_ptr<Tile>> tile_row;
    for( const auto& col : row ){
      Tile* t;
      ETiles e = ETiles(col);
      switch( e ){
        case ETiles::WALL: 
          t = new Wall  (textures[0], 0, 0);  break;
        case ETiles::PATH: 
          t = new Path  (textures[1], 0, 0);  break;
        case ETiles::DOOR: 
          t = new Door   (textures[2], 0, 0);  break;
        case ETiles::SPIKES: 
          t = new Spikes(textures[3], 0, 0);  break;
      }
      tile_row.push_back(  std::shared_ptr<Tile>(t) );
    }
    m_tiles.push_back(tile_row);
  }

  setTilesCoordinates();
}

void TileMap::render(SDL_Renderer *renderer)
{  
  for( int row = 0; row < m_tiles.size(); row++ )
    for( int col = 0; col < m_tiles[0].size(); col++ )
      m_tiles[row][col]->render( renderer );
}

std::list<std::shared_ptr<Tile>> TileMap::getTiles(const Box& box)
{
  if( m_tiles.empty() || m_tiles[0].empty())
    return {};  

  std::list<std::shared_ptr<Tile>> tiles;

  int row_m = m_tiles.size();
  int col_m = m_tiles[0].size();
  for( int row = box.y / tile_h; row < row_m; row++ ){
    for( int col = box.x / tile_w; col < col_m; col++ ){
      if( m_tiles[row][col]->getArea().isColliding( box ) )
        tiles.push_back( m_tiles[row][col] );
    }
  }
  return tiles;
}

int TileMap::getWidth()
{
  if( m_tiles.empty() || m_tiles[0].empty())
    return 0;  
  return m_tiles[0].size() * tile_w; //m_tiles[0].getTexture().getWidth();
}

int TileMap::getHeight()
{
  if( m_tiles.empty() || m_tiles[0].empty())
    return 0;
  return m_tiles.size() * tile_h; //m_tiles[0].getTexture().getHeight();
}

// void TileMap::preRender(SDL_Renderer *renderer)
// {
//   for( int i = 0; i < m_tiles.size(); i++ )
//     for( int j = 0; j < m_tiles[0].size(); j++ )
//       m_tiles[i][j]->render( renderer );
// }

void TileMap::setTilesCoordinates()
{
  for( int row = 0; row < m_tiles.size(); row++ ){
    for( int col = 0; col < m_tiles[0].size(); col++ ){
      double x = tile_w * col;
      double y = tile_h * row;
      m_tiles[row][col]->setCoords( { x, y } );
    }
  }
}

bool TileMap::intersect(const Tile &t, const Box& box)
{
  auto l = t.getArea();
  
  return  l.x < box.x + box.w &&
    l.x + l.w > box.x &&
    l.y < box.y + box.h &&
    l.h + l.y > box.y;
}