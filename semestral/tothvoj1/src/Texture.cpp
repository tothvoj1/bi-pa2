#include "Texture.hpp"

Texture::Texture()
  : m_texture(nullptr), m_width(0), m_height(0)
{
}

Texture::Texture(const std::string &path, SDL_Renderer* renderer_ptr)
  : m_texture(nullptr), m_width(0), m_height(0)
{ 
  if(!loadTexture(path, renderer_ptr))
    throw std::runtime_error( "Unable to load image " + path + "! SDL_image Error: " + IMG_GetError()  );
}

Texture::~Texture()
{
}

bool Texture::loadTexture(const std::string &path,  SDL_Renderer* renderer_ptr)
{
	SDL_Surface* loadedSurface = IMG_Load( path.c_str() );
	if( loadedSurface == NULL )
		return false;

  m_texture = std::shared_ptr<SDL_Texture>(
    SDL_CreateTextureFromSurface( renderer_ptr, loadedSurface ),
    SDL_DestroyTexture
  );
  if( m_texture == NULL ){
		std::cerr << "Unable to create texture from "<< path <<"! SDL_image Error: " << IMG_GetError();
    return false;
  }
  SDL_FreeSurface( loadedSurface );

  SDL_QueryTexture( m_texture.get(), NULL, NULL, &m_width, &m_height );
  
  return true;
}

void Texture::render(int x, int y, SDL_Renderer* r)
{
  SDL_Rect dest = {x,y,m_width,m_height};
  SDL_RenderCopy(r, m_texture.get(), NULL, &dest);
}

int Texture::getWidth() const
{
  return m_width;
}

int Texture::getHeight() const
{
  return m_height;
}
