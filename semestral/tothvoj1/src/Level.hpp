#pragma once

#include "Scene.hpp"
#include "Player.hpp"
#include "Room.hpp"
#include "CollisionHandler.hpp"
#include "ECommands.hpp"
#include <vector>

/// @brief Subclass of Scene handling the actual playthrough. Stores every game object, 
///        controlls the character, updates their properties and renders them on screen.
class Level : public Scene
{
  public:
    Level();
    ~Level();
  
    /// @brief Processes SDL_events. Key events are used to control character, or switching to menu scene.
    /// @retur<n Returns false on SDL_QUIT, true otherwise.
    virtual bool    processInput();
    
    /// @brief Updates coordinates, states of objects. Adds or removes them. 
    /// @return Pointer to itself or a different scene to display (pause menu).
    virtual Scene*  update( const float& tick );
    
    /// @brief Displays all textures on the window
    virtual void    render( SDL_Renderer* renderer );

    Level* setPlayer( std::shared_ptr<Player>& player );
    Level* setRoom( Room * room );
  protected:
    std::vector<SDL_Keycode>  m_key_to_command; //to translate SDL_code to Game event
    CollisionHandler          m_handler;

    std::shared_ptr<Player>   m_adalbert;
    Room*                     m_room;
};

