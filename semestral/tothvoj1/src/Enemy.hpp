#pragma once

#include <list>
#include "Character.hpp"
#include "Projectile.hpp"

/// @brief Base class for enemies. Enemies deal damage 
class Enemy : public Character{
  public:
    Enemy();
    /// @brief Base class for enemies. Notice you have to set target with setTarget method, default target is set to nullptr
    /// @param t Texture for enemy
    /// @param x horizontal position on map
    /// @param y vertical position on map
    /// @param tag tag used to locate textures and other resources
    /// @param speed players speed
    /// @param max_lives default lives
    /// @param m_attack_range radius of how far can enemy reach to deal damage
    /// @param attack_cooldown rest time between hits 
    /// @param damage 
    Enemy(const Texture &t, int x, int y, const std::string& tag, double speed, int max_lives, double attack_range, 
          int attack_cooldown, int attack_damage );
    virtual ~Enemy();

    Enemy& setTarget(const std::shared_ptr<Character>& target);

    int getDamage();

    virtual Enemy& attack( std::list<std::unique_ptr<Projectile>> & projectile_list) = 0;

  protected:
    double      m_attack_range; //how far away from center can enemy be to hurt character
    int         m_attack_cooldown; //how much time enemy needs between each hit
    int         m_attack_cooldown_left; //how much time enemy needs between each hit
    int         m_attack_damage; //how many lives can enemy take away on hit
    
    std::shared_ptr<Character> m_target;
};